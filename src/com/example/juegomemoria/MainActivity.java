package com.example.juegomemoria;

import java.util.Random;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends Activity {
	Button comenzar, reiniciar;
	Chronometer crono;
	int[] posicionesAleatorios = new int[12];
	int idClickeado1 = 0; // guarda el id de la img '1' presionada
	int idClickeado2 = 0; // guarda el id de la img presionada
	int marcaIndiceClickeado = -1;
	int contador = 0;
	//las imagenes
	int[] imagenes = {
			R.drawable.memocharlie, R.drawable.memomarcia,
			R.drawable.memorerun, R.drawable.memosally,
			R.drawable.memoschroeder, R.drawable.memosnoopy,
			R.drawable.memocharlie, R.drawable.memomarcia,
			R.drawable.memorerun, R.drawable.memosally,
			R.drawable.memoschroeder, R.drawable.memosnoopy};
	// generamos punteros para cada ImageButton

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		crono = (Chronometer) findViewById(R.id.chronometer);
		comenzar = (Button) findViewById(R.id.comenzar);
		reiniciar = (Button) findViewById(R.id.reiniciar);
		// enabled buttons
		comenzar.setEnabled(true);
		reiniciar.setEnabled(false);
		// lo sig escucha al cronometro, llega a 30 se detiene y pone cero
		crono.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
			@Override
			public void onChronometerTick(Chronometer arg0) {
				if ((SystemClock.elapsedRealtime() - crono.getBase()) >= 30000) {
					crono.setBase(SystemClock.elapsedRealtime());
					crono.stop();
					comenzar.setEnabled(true);
					reiniciar.setEnabled(false);
					deshabilitarBotones();
				}
			}
		});
		// cantidad de numeros que usaremos para generar num aleatorios
		for (int i = 0; i < 12; i++)
			posicionesAleatorios[i] = i;
		deshabilitarBotones();
	}

	public void comparar(View vista) {
		// Esta funcion realiza la logica para el juego
		//valido que no presione la misma imagen
		contador++;
		String nombreId = vista.getResources().getResourceEntryName(vista.getId());
		nombreId = nombreId.substring(nombreId.length() - 2);
		int indiceAhora = Integer.parseInt(nombreId);

		((ImageButton) findViewById(vista.getId())).setImageResource(imagenes[posicionesAleatorios[indiceAhora]]);// muestra la imagen
		((ImageButton) findViewById(vista.getId())).setEnabled(false);//bloqueamos el boton
		if (marcaIndiceClickeado == -1) {
			// es su 1er click
			idClickeado1 = vista.getId();
			marcaIndiceClickeado = indiceAhora;
		} else {
			// es su 2do click
			if (imagenes[posicionesAleatorios[marcaIndiceClickeado]] != imagenes[posicionesAleatorios[indiceAhora]]){
				mostrarLuegoOcultar(vista);	// son diferentes, entonces oculto las imagenes
				contador-=2;
			}
			marcaIndiceClickeado = -1;//marco en -1 para terminar el ciclo
		}
		if(contador==12){
			Toast.makeText(
					getApplicationContext(),"Felicidades, gano",
					Toast.LENGTH_SHORT).show();
		}
	}

	public void deshabilitarBotones(){
		ImageButton punteroImgs[] = {
				(ImageButton) findViewById(R.id.ImageButton00),
				(ImageButton) findViewById(R.id.ImageButton01),
				(ImageButton) findViewById(R.id.ImageButton02),
				(ImageButton) findViewById(R.id.ImageButton03),
				(ImageButton) findViewById(R.id.ImageButton04),
				(ImageButton) findViewById(R.id.ImageButton05),
				(ImageButton) findViewById(R.id.ImageButton06),
				(ImageButton) findViewById(R.id.ImageButton07),
				(ImageButton) findViewById(R.id.ImageButton08),
				(ImageButton) findViewById(R.id.ImageButton09),
				(ImageButton) findViewById(R.id.ImageButton10),
				(ImageButton) findViewById(R.id.ImageButton11) };
		for(int i= 0; i < 12; i++)
			punteroImgs[i].setEnabled(false);
	}
	public void habilitarBotones(){
		ImageButton punteroImgs[] = {
				(ImageButton) findViewById(R.id.ImageButton00),
				(ImageButton) findViewById(R.id.ImageButton01),
				(ImageButton) findViewById(R.id.ImageButton02),
				(ImageButton) findViewById(R.id.ImageButton03),
				(ImageButton) findViewById(R.id.ImageButton04),
				(ImageButton) findViewById(R.id.ImageButton05),
				(ImageButton) findViewById(R.id.ImageButton06),
				(ImageButton) findViewById(R.id.ImageButton07),
				(ImageButton) findViewById(R.id.ImageButton08),
				(ImageButton) findViewById(R.id.ImageButton09),
				(ImageButton) findViewById(R.id.ImageButton10),
				(ImageButton) findViewById(R.id.ImageButton11) };
		for(int i= 0; i < 12; i++)
			punteroImgs[i].setEnabled(true);
	}
	
	public void desordenarImagenes() {
		// desordenando los elementos con el "Algoritmo de Fisher-Yates"
		Random r = new Random();
		for (int i = posicionesAleatorios.length; i > 0; i--) {
			int posicion = r.nextInt(i);
			int tmp = posicionesAleatorios[i - 1];
			posicionesAleatorios[i - 1] = posicionesAleatorios[posicion];
			posicionesAleatorios[posicion] = tmp;
		}
		ImageButton punteroImgs[] = {
				(ImageButton) findViewById(R.id.ImageButton00),
				(ImageButton) findViewById(R.id.ImageButton01),
				(ImageButton) findViewById(R.id.ImageButton02),
				(ImageButton) findViewById(R.id.ImageButton03),
				(ImageButton) findViewById(R.id.ImageButton04),
				(ImageButton) findViewById(R.id.ImageButton05),
				(ImageButton) findViewById(R.id.ImageButton06),
				(ImageButton) findViewById(R.id.ImageButton07),
				(ImageButton) findViewById(R.id.ImageButton08),
				(ImageButton) findViewById(R.id.ImageButton09),
				(ImageButton) findViewById(R.id.ImageButton10),
				(ImageButton) findViewById(R.id.ImageButton11) };
		// asignamos("seteamos") imagenes audandonos del vector 'posicionesAleatorias'
		for (int i = 0; i < punteroImgs.length; i++)
			punteroImgs[i].setImageResource(R.drawable.star);
	}

	public void comenzar(View vista) {
		// ponemos el cronometro en 0 y empezamos a contar
		crono = (Chronometer) findViewById(R.id.chronometer);
		crono.setBase(SystemClock.elapsedRealtime());
		crono.start();
		// descativamos el boton
		comenzar = (Button) findViewById(R.id.comenzar);
		comenzar.setEnabled(false);
		// activamos el boton de reinicio
		reiniciar = (Button) findViewById(R.id.reiniciar);
		reiniciar.setEnabled(true);
		desordenarImagenes();
		habilitarBotones();
		contador=0;
		idClickeado1 = 0;
		idClickeado2 = 0;
		marcaIndiceClickeado = -1;
	}

	public void mostrarLuegoOcultar(View vista) {
		// Aqui obtenemos el nombre del id, como cada imagen tiene un sufijo de
		// un numero, nos ayudara a encontrar su imagen de origen
		String nombreId = vista.getResources().getResourceEntryName(vista.getId());
		nombreId = nombreId.substring(nombreId.length() - 2);
		int indice = Integer.parseInt(nombreId);

		// cambio la imagen presionada
		((ImageButton) findViewById(vista.getId())).setImageResource(imagenes[posicionesAleatorios[indice]]);
		// guardo el id
		idClickeado2 = vista.getId();
		// Luego muestro por un instante la imagen para luego ocultarla de nuevo
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				//ocultamos
				((ImageButton) findViewById(idClickeado1)).setImageResource(R.drawable.star);// la 1ra
				((ImageButton) findViewById(idClickeado2)).setImageResource(R.drawable.star); // la 2da
				//habilitamos los botones
				((ImageButton) findViewById(idClickeado1)).setEnabled(true);
				((ImageButton) findViewById(idClickeado2)).setEnabled(true);
			}
		}, 200);
	}

	public void reiniciar(View vista) {
		// ponemos el cronometro en 0
		crono = (Chronometer) findViewById(R.id.chronometer);
		crono.setBase(SystemClock.elapsedRealtime());
		crono.stop();
		// descativamos el boton de reinicio
		reiniciar = (Button) findViewById(R.id.reiniciar);
		reiniciar.setEnabled(false);
		// habilitamos el boton de comenzar
		comenzar = (Button) findViewById(R.id.comenzar);
		comenzar.setEnabled(true);
		//deshabilitamos los botones
		deshabilitarBotones();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
